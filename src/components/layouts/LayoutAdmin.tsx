import React, { FC, useEffect } from "react";
import { Link, NavLink } from "react-router-dom";
import styles from "./LayoutAdmin.module.css";

const { VITE_APP_NAME } = import.meta.env;

const LayoutAdmin: FC = ({ children }) => {
	useEffect(() => {
		// document.title = VITE_APP_NAME;
	}, []);

	return (
		<main>
			{/* //? MAIN NAV */}
			<nav className={styles["main-nav"]}>
				<a className={styles["main-nav__brand"]} href="/">
					{VITE_APP_NAME}
				</a>
				<ul className={styles["main-nav__options"]}>
          <li className={styles["main-nav__option"]}>
            <NavLink to="/admin/projects" className={({ isActive }) => (isActive ? styles["link-active"] : styles["link"])} >Proyectos</NavLink>
					</li>
					<li className={styles["main-nav__option"]}>
						<NavLink to="/admin/categories" className={({ isActive }) => (isActive ? styles["link-active"] : styles["link"])}>Categorías</NavLink>
					</li>
				</ul>
			</nav>

			{/* //? CONTENT */}
			<div className={styles["content"]}>{children}</div>
		</main>
	);
};

export default LayoutAdmin;
