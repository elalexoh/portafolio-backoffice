import React, { FC, useContext, useEffect, useState } from "react";
import { CategoriesContext } from "../../context/categories-context";
import Button from "../ui/Button";
import Card from "../ui/Card";
import styles from "./CategoriesList.module.css";

const CategoriesList: FC = () => {
	const { getCategories, categories, deleteCategory } =
		useContext(CategoriesContext);

	useEffect(() => {
		getCategories();
	}, []);

	// const categoriesList = ;
	return (
		<>
			{categories.map((category) => (
				<Card key={category.id}>
					<div className={styles["category"]}>
						<p className={styles["category__name"]}>
							{category.name} <small>({category.slug})</small>
						</p>
						<div className={styles["category__options"]}>
							<Button
								onClick={() => deleteCategory(category.id)}
								bgColor="coral"
							>
								Eliminar
							</Button>
						</div>
					</div>
				</Card>
			))}
		</>
	);
};

export default CategoriesList;
