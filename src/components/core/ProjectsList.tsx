import React, { Fragment, useContext, useEffect } from "react";
import { CategoriesContext } from "../../context/categories-context";
import { ProjectsContext } from "../../context/projects-context";
import Button from "../ui/Button";
import Card from "../ui/Card";
import codeIcon from "../../assets/images/icons/code.svg";
import styles from "./ProjectsList.module.css";

const ProjectsList = () => {
	const { projects, getProjects, deleteProject, onEditMode } =
		useContext(ProjectsContext);
	const { categories } = useContext(CategoriesContext);

	useEffect(() => {
		getProjects();
	}, []);

	const getCategoryName = (id: string) => {
		if (categories.length) {
			const category = categories.filter((category) => category.id === id);
			return category[0].name;
		} else {
			return "";
		}
	};

	const projectsList = projects.map((project) => (
		<Card
			key={`${project.title}__${project.category}`}
			title={`${project.title} (${getCategoryName(project.category)})`}
			titleAction={
				<div>
					<a
						href="#"
						style={{ color: "teal" }}
            onClick={() => onEditMode(project.id)}
            title="Editar"
					>
						&#9998;
					</a>
					<a
						className={styles["link"]}
						href={project.link}
						target="_blank"
						style={{ color: "teal" }}
						title={`${project.title} repositorio`}
					>
						<img src={codeIcon} alt="" />
					</a>
				</div>
			}
		>
			<div className={styles["project-card"]}>
				<p className={styles["description"]}>{project.description}</p>
				<Button bgColor="coral" onClick={() => deleteProject(project.id!)}>
					Eliminar
				</Button>
			</div>
		</Card>
	));
	return <Fragment>{projectsList}</Fragment>;
};

export default ProjectsList;
