import React, { useContext, useRef } from "react";
import { CategoriesContext } from "../../context/categories-context";
import Button from "../ui/Button";
import Card from "../ui/Card";

const newCategory = () => {
	const { getCategories } = useContext(CategoriesContext);
	const nameRef = useRef<HTMLInputElement>(null);
	const slugRef = useRef<HTMLInputElement>(null);

	const handleSubmit = async (e: React.SyntheticEvent) => {
		e.preventDefault();
		await fetch(
			"https://deeplyportfolio-default-rtdb.firebaseio.com/categories.json",
			{
				method: "POST",
				body: JSON.stringify({
					name: nameRef.current!.value,
					slug: slugRef.current!.value,
				}),
			}
		)
			.then((res) => {
				res.json();
				console.debug("json", res);
			})
			.then((data) => {
				getCategories();
			})
			.catch((error) => {
				console.debug("Ha ocurrido un error al crear la categoría");
			});
	};
	return (
		<Card title="Nueva Categoría">
			<form action="" onSubmit={handleSubmit}>
				{/* Nombre */}
				<div className="input-wrapper">
					<label htmlFor="name">Nombre</label>
					<input type="text" name="" id="name" ref={nameRef} />
				</div>
				{/* Slug */}
				<div className="input-wrapper">
					<label htmlFor="slug">Slug</label>
					<input type="text" name="" id="slug" ref={slugRef} />
				</div>
				<Button bgColor="teal" type="submit">
					Enviar
				</Button>
			</form>
		</Card>
	);
};

export default newCategory;
