import React, {
	ChangeEvent,
	ChangeEventHandler,
	useContext,
	useEffect,
	useRef,
	useState,
} from "react";
import { CategoriesContext } from "../../context/categories-context";
import { ProjectsContext } from "../../context/projects-context";
import Card from "../ui/Card";
import storage from "../../firebase";
import styles from "./NewProject.module.css";

// GLOBAL vars
const { VITE_BASE_URL } = import.meta.env;

interface HTMLInputEvent extends Event {
	target: HTMLInputElement & EventTarget;
}

const NewProject = () => {
	const { getCategories, categories } = useContext(CategoriesContext);
	const { getProjects, editMode, projectToEdit, offEditMode } =
		useContext(ProjectsContext);

	const titleRef = useRef<HTMLInputElement>(null);
	const descriptionRef = useRef<HTMLTextAreaElement>(null);
	const categoryRef = useRef<HTMLSelectElement>(null);
	const typeRef = useRef<HTMLSelectElement>(null);
	const postLinkRef = useRef<HTMLInputElement>(null);

	const [image, setImage] = useState<File | undefined>();
	const [imageUrl, setImageUrl] = useState<
		string | undefined | HTMLImageElement
	>();
	const [projectId, setProjectId] = useState<string>();

	useEffect(() => {
		getCategories();
	}, []);

	useEffect(() => {
		if (editMode) {
			setProjectId(projectToEdit!.id);
			titleRef.current!.value = projectToEdit!.title;
			descriptionRef.current!.value = projectToEdit!.description;
			categoryRef.current!.value = projectToEdit!.category;
			typeRef.current!.value = projectToEdit!.type;
			postLinkRef.current!.value = projectToEdit!.link;
			setImageUrl(projectToEdit!.image);
			console.table(projectToEdit);
		} else {
			titleRef.current!.value = "";
			descriptionRef.current!.value = "";
			categoryRef.current!.value = "";
			typeRef.current!.value = "";
			postLinkRef.current!.value = "";
		}
	}, [projectToEdit]);

	const uploadHandler = async () => {
		if (image == undefined) return;
		console.debug(image.name);
		await storage.ref(`/images/${image.name}`).put(image);

		const url = await storage.ref(`images/${image.name}`).getDownloadURL();
		return url;
	};

	const handleSubmit = async (e: React.FormEvent) => {
		e.preventDefault();
		let url = `${VITE_BASE_URL}/projects.json`;
    let method = "POST";
    
		if (editMode) {
			url = `${VITE_BASE_URL}/projects/${projectId}.json`;
			method = "PUT";
		}

		try {
			const imgUrl = await uploadHandler();
			await fetch(url, {
				method: method,
				body: JSON.stringify({
					title: titleRef.current!.value,
					description: descriptionRef.current!.value,
					category: categoryRef.current!.value,
					link: postLinkRef.current!.value,
					type: typeRef.current!.value,
					image: imgUrl,
				}),
			})
				.then((res) => res.json)
				.then((data) => getProjects())
				.catch((error) =>
					console.error("ha ocurrido un error al crear el proyecto", error)
				);
		} catch (error) {
			console.error("ha ocurrido un error en HandleSubmit");
		}
	};

	const imageHandleChange = (event: React.ChangeEvent) => {
		const target = event.target as HTMLInputElement;
		const file = target.files![0];
		setImage(file);
	};

	const title = editMode
		? `Editando "${projectToEdit!.title}"`
		: "Nuevo Proyecto";

	const actions = editMode ? (
		<div>
			<a href="#" style={{ color: "coral" }} onClick={() => offEditMode()}>
				cancel
			</a>
		</div>
	) : null;

	const projectThumbnail = editMode ? (
		<div className={styles["thumbnail-preview"]}>
			<img className={styles["thumbnail-preview__img"]} src={imageUrl} alt="" />
			<small className={styles["thumbnail-preview__label"]}>
				Esta es su antigua imagen
			</small>
		</div>
	) : (
		false
	);

	return (
		<Card title={title} titleAction={actions}>
			<form onSubmit={handleSubmit}>
				{/* Titulo */}
				<div className="input-wrapper">
					<label htmlFor="title">Título</label>
					<input type="text" id="title" ref={titleRef} />
				</div>

				{/* descripción */}
				<div className="input-wrapper">
					<label htmlFor="description">Descripción</label>
					<textarea name="" id="description" ref={descriptionRef}></textarea>
				</div>

				{/* Tipo de proyecto */}
				<div className="input-wrapper">
					<label htmlFor="project-type">Tipo de proyecto</label>
					<select name="" id="project-type" ref={typeRef}>
						<option defaultValue="" disabled>
							--Selecciona el tipo de proyecto--
						</option>
						<option value="blog">Blog</option>
						<option value="spa">Single Page Aplication</option>
						<option value="ecommerce">Ecommerce</option>
						<option value="otro">Otro</option>
					</select>
				</div>

				{/* categoría */}
				<div className="input-wrapper">
					<label htmlFor="categories">Categorías</label>
					<select name="" id="categories" ref={categoryRef}>
						<option defaultValue="" disabled>
							--Selecciona la categoría--
						</option>
						{categories.map((category) => (
							<option key={category.id} value={category.id}>
								{category.name}
							</option>
						))}
					</select>
				</div>

				{/* enlace */}
				<div className="input-wrapper">
					<label htmlFor="link">Enlace (Github, Gitlab, Codepen)</label>
					<input type="text" name="" id="link" ref={postLinkRef} />
				</div>

				{/* imagen */}
				{projectThumbnail}
				<div className="input-wrapper">
					<input type="file" onChange={imageHandleChange} />
				</div>

				{/* is online */}
				<button type="submit">Enviar</button>
			</form>
		</Card>
	);
};

export default NewProject;
