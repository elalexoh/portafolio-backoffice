import React, { ButtonHTMLAttributes, FC, useEffect } from "react";
import styles from "./Button.module.css";

enum ButtonTypes {
	"button",
	"submit",
	"reset",
	undefined,
}

const Button: FC<{
	bgColor: string;
	onClick?: () => void;
	type?: "button" | "submit" | "reset" | undefined;
}> = ({ bgColor, children, onClick, type }) => {
	return (
		<button
			className={styles["button"]}
			style={{ backgroundColor: bgColor }}
			onClick={onClick}
			type={type}
		>
			{children}
		</button>
	);
};

export default Button;
