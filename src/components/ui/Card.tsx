import React, { FC, ReactElement } from "react";
import styles from "./Card.module.css";

const Card: FC<{
	title?: string;
	titleAction?: ReactElement | null;
}> = ({ children, title, titleAction }) => {
	return (
		<div className={styles["card-wrapper"]}>
			{title && (
				<div className={styles["card-wrapper__header"]}>
					<h2 className={styles["card-wrapper__title"]}>{title}</h2>
					{titleAction}
				</div>
			)}
			<div className={styles["card-wrapper__content"]}>{children}</div>
		</div>
	);
};

export default Card;
