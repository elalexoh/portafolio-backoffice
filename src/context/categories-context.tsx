import React, { useState, FC } from "react";
import Category from "../models/category";

const { VITE_BASE_URL } = import.meta.env;

type CategoriesContext = {
	categories: Category[];
	addCategory?: (category: Category) => void;
	getCategories: () => void;
	deleteCategory: (id: string) => void;
};

const defaultState = {
	categories: [],
	addCategory: () => {},
	getCategories: () => {},
	deleteCategory: () => {},
};

export const CategoriesContext =
	React.createContext<CategoriesContext>(defaultState);

export const CategoriesProvider: FC = ({ children }) => {
	const [categories, setCategories] = useState<Category[]>([]);

	const getCategories = () => {
		fetch(`${VITE_BASE_URL}/categories.json`)
			.then((response) => response.json())
			.then((data) => {
				//? REESTRUCTURANDO RESPUESTA DE FIREBASE
				let categories = [];
				for (const key in data) {
					categories.push({
						id: key,
						name: data[key].name,
						slug: data[key].slug,
					});
				}
				setCategories(categories);
			})
			.catch((error) => {
				console.error("Ha ocurrido un error al obtener las categorías ");
			});
	};

	const deleteCategory = (id: string) => {
		fetch(`${VITE_BASE_URL}/categories/${id}.json`, {
			method: "DELETE",
		})
			.then((response) => response.json())
			.then((data) => {
				getCategories();
			})
			.catch((error) => {
				console.error("ha ocurrido un error al eliminar");
			});
	};

	const addCategory = (category: Category) => {
		setCategories((prevCategories) => [...prevCategories, category]);
	};

	return (
		<CategoriesContext.Provider
			value={{
				categories,
				getCategories,
				deleteCategory,
			}}
		>
			{children}
		</CategoriesContext.Provider>
	);
};
