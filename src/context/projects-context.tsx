import React, { createContext, FC, useState } from "react";
import { IProject, IProjectCtx } from "../models/project";

const defaultState = {
	projects: [],
	getProjects: () => {},
	deleteProject: () => {},
	editMode: false,
	onEditMode: () => {},
	offEditMode: () => {},
	projectToEdit: null,
};

const { VITE_BASE_URL } = import.meta.env;

export const ProjectsContext = createContext<IProjectCtx>(defaultState);

export const ProjectsProvider: FC = ({ children }) => {
	const [projects, setProjects] = useState<IProject[]>([]);
	const [editMode, setEditMode] = useState<boolean>(false);
	const [projectToEdit, setProjectToEdit] = useState<IProject | null>(null);

	const getProjects = async () => {
		await fetch(`${VITE_BASE_URL}/projects.json`)
			.then((res) => res.json())
			.then((data) => {
				//? REESTRUCTURANDO RESPUESTA DE FIREBASE
				let projects = [];
				for (const key in data) {
					projects.push({
						id: key,
						title: data[key].title,
						description: data[key].description,
						category: data[key].category,
						link: data[key].link,
						image: data[key].image,
						type: data[key].type,
					});
				}
				setProjects(projects);
			})
			.catch((error) =>
				console.error("ha ocurrido un error al obtener los proyectos", error)
			);
	};

	const deleteProject = async (id: string) => {
		await fetch(`${VITE_BASE_URL}/projects/${id}.json`, {
			method: "DELETE",
		})
			.then((res) => res.json())
			.then((data) => getProjects())
			.catch((error) =>
				console.error("ha ocurrido un error al eliminar el proyecto", error)
			);
	};

	const isEditMode = () => {
		console.debug(editMode);
	};

	const togglingEditMode = () => {
		setEditMode((prevState) => (prevState = !prevState));
	};

	const onEditMode = (id: string) => {
		// turn on Edit Mode
		setEditMode((prevState) => (prevState = true));
		const toEdit = projects.filter((project) => project.id === id);
		setProjectToEdit(toEdit[0]);
	};
	const offEditMode = () => {
		// turn off Edit mode
		setEditMode((prevState) => (prevState = false));
		setProjectToEdit(null);
		console.debug("offEditMode");
	};

	return (
		<ProjectsContext.Provider
			value={{
				projects,
				getProjects,
				deleteProject,
				editMode,
				onEditMode,
				offEditMode,
				projectToEdit,
			}}
		>
			{children}
		</ProjectsContext.Provider>
	);
};
