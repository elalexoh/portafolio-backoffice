type Category = {
  id: string,
  name: string,
  slug: string
}
export default Category