export type IProject = {
  id?: string;
  title: string;
  description: string;
  category: string;
  link: string;
  image?: HTMLImageElement | string;
  type: string
};
export type IProjectCtx = {
  projects: IProject[],
  getProjects: () => void
  deleteProject: (id: string) => void;
  editMode: boolean;
  togglingEditMode?: () => void;
  onEditMode: (id: string) => void;
  offEditMode: () => void;
  projectToEdit: IProject | null
}