import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import Admin from "./pages/admin/index";
import Projects from "./pages/admin/Projects";
import Client from "./pages/client/index";
import Categories from "./pages/admin/Categories";
import { CategoriesProvider } from "./context/categories-context";
import { ProjectsProvider } from "./context/projects-context";

ReactDOM.render(
	<ProjectsProvider>
		<CategoriesProvider>
			<BrowserRouter>
				<Routes>
					<Route path="/admin" element={<Projects />} />
					<Route path="/admin/projects" element={<Projects />} />
					<Route path="/admin/categories" element={<Categories />} />
					<Route path="/" element={<Projects />} />
				</Routes>
			</BrowserRouter>
		</CategoriesProvider>
	</ProjectsProvider>,
	document.getElementById("root")
);
