import firebase from "firebase";


const firebaseConfig = {
  apiKey: "AIzaSyCeYiym6xd5qtL0MylYrf_JgUFOc9_XtaE",

  authDomain: "deeplyportfolio.firebaseapp.com",

  databaseURL: "https://deeplyportfolio-default-rtdb.firebaseio.com",

  projectId: "deeplyportfolio",

  storageBucket: "deeplyportfolio.appspot.com",

  messagingSenderId: "207296453063",

  appId: "1:207296453063:web:5758cad3d3c20cb42b21e2",
};

firebase.initializeApp(firebaseConfig);

let storage = firebase.storage();


export default storage;
