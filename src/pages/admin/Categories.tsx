import CategoriesList from "../../components/core/CategoriesList";
import NewCategory from "../../components/core/NewCategory";
import LayoutAdmin from "../../components/layouts/LayoutAdmin";
import Card from "../../components/ui/Card";

const Categories = () => {
	return (
		<LayoutAdmin>
			<div className="container">
				<div className="row">
					<div className="col-xs-8">
						<Card title="Mis Categorías">
							<CategoriesList />
						</Card>
					</div>
					<div className="col-xs-4">
						<NewCategory />
					</div>
				</div>
			</div>
		</LayoutAdmin>
	);
};

export default Categories;
