import React, { useState } from "react";
import NewProject from "../../components/core/NewProject";
import ProjectsList from "../../components/core/ProjectsList";
import LayoutAdmin from "../../components/layouts/LayoutAdmin";

const Projects = () => {
	return (
		<LayoutAdmin>
			<div className="container">
				<div className="row">
					<div className="col-xs-7">
						<ProjectsList />
					</div>
					<div className="col-xs-5">
						<NewProject />
					</div>
				</div>
			</div>
		</LayoutAdmin>
	);
};

export default Projects;
