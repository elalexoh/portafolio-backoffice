import React from "react";
import { Navigate, Route } from "react-router-dom";
import NewCategory from "../../components/core/NewCategory";
import NewPost from "../../components/core/NewProject";
import LayoutAdmin from "../../components/layouts/LayoutAdmin";

export const index = () => {
	return (
		<LayoutAdmin>
			<NewPost />
			<NewCategory />
		</LayoutAdmin>
	);
};
export default index;
