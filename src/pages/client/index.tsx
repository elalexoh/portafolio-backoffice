import React, { useEffect, useRef } from "react";

const index = () => {
	const canvas = useRef<HTMLCanvasElement>(null);

	useEffect(() => {
		initCanvas();
	}, []);

	const initCanvas = () => {
		let c: any = canvas.current;
		let ctx = c.getContext("2d");
		let s = getComputedStyle(c);
		let w = Number(s.width.split("px")[0]);
		let h = Number(s.height.split("px")[0]);
		let constant = 0.52 * 4; //? 0.52 equivale a 10px
		c.width = w;
		c.height = h;

		let y1 = (48.05 * h) / 100;
		let y2 = (89.84 * h) / 100;

		let y3 = ((48.05 - constant) * h) / 100;
		let y4 = ((89.84 - constant) * h) / 100;

		let cpx1 = (32.23 * w) / 100;
		let cpy1 = (30 * h) / 100;

		let cpx2 = (25 * w) / 100;
		let cpy2 = (135 * h) / 100;

		ctx.beginPath();
		ctx.moveTo(0, y1);
		ctx.bezierCurveTo(cpx1, cpy1, cpx2, cpy2, w, y2);
		ctx.stroke();

		ctx.beginPath();
		ctx.moveTo(0, y3);
		ctx.bezierCurveTo(0, y3, w, y4, w, y4);
		ctx.stroke();

		console.debug(w, h);
	};

	const sinuous_two = () => {
		// let c = canvas.current;
		// let counter = 20;
		// // 25, 100, 75, 0,
		// let x1 = 25,
		// 	y1 = 0,
		// 	x2 = 75,
		// 	y2 = 0;
		// for (let i = 0; i <= 200; i += counter) {
		// 	x1 += 0;
		// 	y1 += counter;
		// 	x2 += 0;
		// 	y2 += counter;
		// 	let c = canvas.current;
		// 	let ctx = c.getContext("2d");
		// 	ctx.beginPath();
		// 	ctx.moveTo(0, i);
		// 	ctx.bezierCurveTo(0, y1, 200, y2, 200, i);
		// 	ctx.stroke();
		// 	ctx.fillStyle = "#bada55";
		// 	ctx.beginPath();
		// 	ctx.arc(x1, y1, 3, 0, 2 * Math.PI, true);
		// 	ctx.fill();
		// 	ctx.fillStyle = "#ffffff";
		// 	ctx.beginPath();
		// 	ctx.arc(x2, y2, 3, 0, 2 * Math.PI, true);
		// 	ctx.fill();
		// 	console.debug(y1, y2);
		// }
	};

	return (
		<div>
			<canvas id="canvas" ref={canvas} width="200" height="200"></canvas>
			<section>
				Lorem ipsum dolor sit amet consectetur adipisicing elit. Voluptas fugiat
				eaque mollitia autem ut ipsa temporibus? Modi repudiandae ea, natus
				doloremque cumque ipsum adipisci eaque alias, molestiae, perferendis in
				hic.
			</section>
		</div>
	);
};

export default index;
